<?php

declare(strict_types=1);

namespace Option2;

class CardController
{
    private CardService $cardService;

    /**
     * @Route("/{cardIdDto}", methods={"DELETE"})
     * @ParamConverter("cardIdDto", converter="dto", class="CardIdDto")
     * @OA\Response(response=200, description="Card deleted")
     * @OA\Response(response=404, description="Card not found")
     */
    public function deleteCard(CardIdDto $cardIdDto): \CardEntity
    {
        return $this->cardService->deleteCard($cardIdDto->cardId);
    }

    /**
     * @Route("/{cardIdDto}/rename", methods={"PUT"})
     * @ParamConverter("cardIdDto", converter="dto", class="CardIdDto")
     * @OA\Response(response=200, description="Card renamed")
     * @OA\Response(response=404, description="Card not found")
     */
    public function renameCard(CardIdDto $cardIdDto, CardRenameDto $cardRenameDto): \CardEntity
    {
        return $this->cardService->renameCard($cardIdDto->cardId, $cardRenameDto->name);
    }
}
