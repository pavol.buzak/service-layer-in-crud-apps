<?php

declare(strict_types=1);

namespace Option2;


class CardService
{
    private $entityManger;

    private $validator;

    public function deleteCard(int $id): \CardEntity
    {
        $cardRepository = $this->entityManger->getRepository(\CardEntity::class);

        $card = $cardRepository->get($id);
        if ($card === null) {
            throw new \RuntimeException('Entity doesn\'t exists');
        }
        \assert($card instanceof \CardEntity);

        $this->entityManger->remove($card);
        $this->entityManger->flush();

        return $card;
    }

    public function renameCard(int $id, string $name): \CardEntity
    {
        $cardRepository = $this->entityManger->getRepository(\CardEntity::class);

        $card = $cardRepository->get($id);
        if ($card === null) {
            throw new \RuntimeException('Entity doesn\'t exists');
        }
        \assert($card instanceof \CardEntity);

        $card->setName($name);

        $errors = $this->validator->validate($card);
        if ($errors) {
            throw new \RuntimeException('There is some errors'); // I don't know how to list those errors in response
        }

        $this->entityManger->flush();

        return $card;
    }
}
