<?php

declare(strict_types=1);

namespace Option2;

class CardIdDto
{
    public $cardId;
}
