<?php

declare(strict_types=1);

namespace Option2;

class CardRenameDto
{
    /**
     * @Assert\NotNull
     * @Assert\NotBlank
     * @Groups({"api"})
     * @var string
     */
    public $name;
}
