<?php

declare(strict_types=1);

class CardEntity
{
    private int $id;

    /**
     * @Assert\NotNull
     * @Assert\NotBlank
     * @Assert\Length(min = 4)
     * @Groups({"api"})
     */
    private string $name;

    public function setName(string $name): void
    {
        $this->name = $name;
    }
}
