<?php

declare(strict_types=1);

namespace Option1;

class CardService
{
    private $entityManger;

    public function deleteCard(\CardEntity $card): void
    {
        $this->entityManger->remove($card);
        $this->entityManger->flush();
    }

    public function renameCard(\CardEntity $card, string $name): void
    {
        $card->setName($name);
        $this->entityManger->flush();
    }
}
