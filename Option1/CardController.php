<?php

declare(strict_types=1);

namespace Option1;

class CardController
{
    private CardService $cardService;

    private $validator;

    /**
     * @Route("/{id}", methods={"DELETE"})
     * @ParamConverter("card", converter="doctrine.orm", class="CardEntity")
     * @OA\Response(response=200, description="Card deleted")
     * @OA\Response(response=404, description="Card not found")
     */
    public function deleteCard(\CardEntity $card): \CardEntity
    {
        $this->cardService->deleteCard($card);

        return $card;
    }

    /**
     * @Route("/{id}/rename", methods={"PUT"})
     * @ParamConverter("card", converter="doctrine.orm", class="CardEntity")
     * @OA\Response(response=200, description="Card renamed")
     * @OA\Response(response=400, description="Error while renaming")
     * @OA\Response(response=404, description="Card not found")
     */
    public function renameCard(\CardEntity $card, string $name): \CardEntity
    {
        $card->setName($name);
        $errors = $this->validator->validate($card);
        if ($errors) {
            return new JsonResponse([
                'errors' => $errors
            ]);
        }

        $this->entityManger->flush();

        return $card;
    }
}
